import { VantComponent } from '@@/common';

const map = {
  "spinner": "\ue69f",
  "circular": "\ue75a"
};

export default VantComponent({
  name: 'icon',
  props: {
    name: String,
    dot: {
      type: Boolean,
      default: false,
    },
    badge: [Number, String],
    color: String,
    className: String,
    wrapClassName: String,
    size: {
      type: Number,
      default: 30,
    },
  },
  computed: {
    content() {
      return map[this.name];
    },
  },
  methods: {
    handClick() {
      this.$emit('tap');
    },
  },
});
