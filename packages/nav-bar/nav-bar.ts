import { VantComponent } from '../common/component';

export default VantComponent({
  name: 'nav-bar',

  props: {
    title: String,
    fixed: {
      type: Boolean,
      default: false,
      // observer: 'setHeight',
    },
    placeholder: {
      type: Boolean,
      default: false,
      // observer: 'setHeight',
    },
    leftText: String,
    rightText: String,
    customStyle: String,
    leftArrow: Boolean,
    border: {
      type: Boolean,
      default: true,
    },
    zIndex: {
      type: Number,
      default: 1,
    },
    safeAreaInsetTop: {
      type: Boolean,
      default: true,
    },
  },

  data: {
    height: 46,
  },

  methods: {
    onClickLeft() {
      this.$emit('clickLeft');
    },

    onClickRight() {
      this.$emit('clickRight');
    },

    setHeight() {
      if (!this.fixed || !this.placeholder) {
        return;
      }

      // wx.nextTick(() => {
      //   getRect(this, '.van-nav-bar').then((res) => {
      //     if (res && 'height' in res) {
      //       this.setData({ height: res.height });
      //     }
      //   });
      // });
    },
  },
});
