import uniqueId from 'lodash/uniqueId';
import { global } from './global';
import {
  DataOption,
  PropertyOption,
  MethodOption,
  BehaviorOptions,
} from '@@/definitions';

/** 注册一个 `behavior`，接受一个 `Object` 类型的参数。*/
export function Behavior<
  TData extends DataOption,
  TProperty extends PropertyOption,
  TMethod extends MethodOption
>(options: BehaviorOptions<TData, TProperty, TMethod>): string {
  const behaviorId = uniqueId();
  global.behaviors[behaviorId] = options;
  return behaviorId;
}
