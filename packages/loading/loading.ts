import { VantComponent } from '@@/common';

export default VantComponent({
  name: 'loading',
  props: {
    color: {
      type: String,
      default: '#c8c9cc',
    },
    vertical: Boolean,
    type: {
      type: String,
      default: 'circular',
    },
    size: {
      type: Number,
      default: 30,
    },
    textSize: String,
  },
  data: {
    array12: Array.from({ length: 12 }),
  },
});
