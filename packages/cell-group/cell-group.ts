import { VantComponent } from '@@/common';

export default VantComponent({
  name: 'cell-group',
  props: {
    title: String,
    border: {
      type: Boolean,
      default: true,
    },
    inset: Boolean,
  },
});
