import { EventEmitter } from '@@/utils';
import { IAnyObject, EEValue, BehaviorOptions } from '@@/definitions';

interface GlobalType {
  event$: EventEmitter<EEValue>;
  behaviors: Record<
    string,
    BehaviorOptions<IAnyObject, IAnyObject, IAnyObject>
  >;
}

export const global: GlobalType = {
  behaviors: {},
  event$: new EventEmitter<EEValue>(),
};
