const gulp = require('gulp');
const path = require('path');
const less = require('gulp-less');
const insert = require('gulp-insert');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const src = path.resolve(__dirname, '../packages');

const libConfig = path.resolve(__dirname, '../tsconfig.lib.json');

const libDir = path.resolve(__dirname, '../lib');

const baseCssPath = path.resolve(__dirname, '../packages/common/index.css');

const lessCompiler = (dist) =>
  function compileLess() {
    return gulp
      .src(`${src}/**/*.less`)
      .pipe(less())
      .pipe(
        insert.transform((contents, file) => {
          if (!file.path.includes('packages' + path.sep + 'common')) {
            const relativePath = path
              .relative(
                path.normalize(`${file.path}${path.sep}..`),
                baseCssPath
              )
              .replace(/\\/g, '/');
            contents = `@import '${relativePath}';
${contents}`;
          }
          return contents;
        })
      )
      .pipe(gulp.dest(dist));
  };

const tsCompiler = (dist, config) =>
  async function compileTs() {
    await exec(`npx tsc -p ${config}`);
    await exec(`npx tscpaths -p ${config} -s ../packages -o ${dist}`);
  };

const copier = (dist, ext) =>
  async function copy() {
    return gulp.src(`${src}/**/*.${ext}`).pipe(gulp.dest(dist));
  };

const staticCopier = (dist) =>
  gulp.parallel(copier(dist, 'hml'), copier(dist, 'ttf'));

const cleaner = (path) =>
  function clean() {
    return exec(`npx rimraf ${path}`);
  };

const tasks = [['buildLib', libDir, libConfig]].reduce(
  (prev, [name, ...args]) => {
    prev[name] = gulp.series(
      cleaner(...args),
      gulp.parallel(
        tsCompiler(...args),
        lessCompiler(...args),
        staticCopier(...args)
      )
    );
    return prev;
  },
  {}
);

tasks.buildExample = gulp.series(
  cleaner(libDir),
  gulp.parallel(
    tsCompiler(libDir, libConfig),
    lessCompiler(libDir),
    staticCopier(libDir),
    () => {
      gulp.watch(`${src}/**/*.ts`, tsCompiler(libDir, libConfig));
      gulp.watch(`${src}/**/*.less`, lessCompiler(libDir));
      gulp.watch(`${src}/**/*.hml`, copier(libDir, 'hml'));
    }
  )
);

module.exports = tasks;
