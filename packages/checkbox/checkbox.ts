import { VantComponent, useParent } from '@@/common';
import { TrivialInstance } from '@@/definitions';

function emit(target: TrivialInstance, value: boolean | any[]) {
  target.$emit('input', value);
  target.$emit('change', value);
}

export default VantComponent({
  name: 'checkbox',
  field: true,

  relation: useParent('checkbox-group'),

  props: {
    value: Boolean,
    disabled: Boolean,
    useIconSlot: Boolean,
    checkedColor: String,
    labelPosition: {
      type: String,
      default: 'right',
    },
    labelDisabled: Boolean,
    shape: {
      type: String,
      default: 'round',
    },
    iconSize: {
      type: null,
      default: 20,
    },
  },

  data: {
    parentDisabled: false,
    direction: 'vertical',
  },

  methods: {
    emitChange(value: boolean) {
      if (this.parent$) {
        this.setParentValue(this.parent$, value);
      } else {
        emit(this, value);
      }
    },

    toggle() {
      const { parentDisabled, disabled, value } = this;
      if (!disabled && !parentDisabled) {
        this.emitChange(!value);
      }
    },

    onTapLabel() {
      const { labelDisabled, parentDisabled, disabled, value } = this;
      if (!disabled && !labelDisabled && !parentDisabled) {
        this.emitChange(!value);
      }
    },

    setParentValue(parent: TrivialInstance, value: boolean) {
      const parentValue = parent.value.slice();
      const { name } = this;
      const { max } = parent;

      if (value) {
        if (max && parentValue.length >= max) {
          return;
        }

        if (parentValue.indexOf(name) === -1) {
          parentValue.push(name);
          emit(parent, parentValue);
        }
      } else {
        const index = parentValue.indexOf(name);
        if (index !== -1) {
          parentValue.splice(index, 1);
          emit(parent, parentValue);
        }
      }
    },
  },
});
