import Animator, { AnimatorOptions } from '@ohos.animator';
import { VantComponent } from '@@/common';

export default VantComponent({
  name: 'overlay',
  props: {
    visible: Boolean,
    duration: {
      type: Number,
      default: 300,
    },
    zIndex: {
      type: Number,
      default: 1,
    },
    lockScroll: {
      type: Boolean,
      default: true,
    },
  },

  data() {
    return {
      display: false,
      opacity: 0,
      animatorOptions: {} as AnimatorOptions,
      animator: null as unknown as Animator,
    };
  },

  lifetimes: {
    onInit() {
      this.animatorOptions = {
        duration: this.duration,
        easing: 'ease',
        fill: 'forwards',
        iterations: 1,
        begin: 0.0,
        end: 1.0,
      };
      this.animator = Animator.createAnimator(this.animatorOptions);
      var _this = this;
      this.animator.onframe = function (timestamp) {
        _this.setData$({ opacity: timestamp });
      };
      this.animator.onfinish = function () {
          if (!_this.visible) _this.setData$({ opacity: 0, display: false });
      };

      // if (this.visible === true) {
      //   this.observeShow(true, false);
      // }

      this.$watch('visible', 'observeShow');
    },
  },

  methods: {
    observeShow(value: boolean, old: boolean) {

      if (value === old) {
        return;
      }

      value ? this.enter() : this.leave();
    },

    enter() {
      this.setData$({ display: true });
      this.animatorOptions.begin = 0;
      this.animatorOptions.end = 1;
      this.animator.update(this.animatorOptions);
      this.animator.play();
    },
    leave() {
      this.animatorOptions.begin = 1;
      this.animatorOptions.end = 0;
      this.animator.update(this.animatorOptions);
      this.animator.play();
    },

    handClick(event) {
      this.$emit('tap');
    },

    // for prevent touchmove
    noop() {},
  },
});
