declare module '@ohos.animator' {
  export interface AnimatorOptions {
    /** 动画播放的时长，单位毫秒，默认为0。 */
    duration?: number;
    easing?: string;
    delay?: number;
    fill?: string;
    direction?: string;
    iterations?: number;
    begin?: number;
    end?: number;
  }
  export default class Animator {
    constructor(private options: AnimatorOptions) {}

    /** 过程中可以使用这个接口更新动画参数，入参与createAnimator一致 */
    update(options: AnimatorOptions): void;
    /** 开始动画 */
    play(): void;
    /** 结束动画 */
    finish(): void;
    /** 暂停动画 */
    pause(): void;
    /** 取消动画 */
    cancel(): void;
    /** 倒播动画 */
    reverse(): void;
    /** 逐帧插值回调事件，入参为当前帧的插值 */
    onframe(value: number): void;
    onfinish(): void;
    oncancel(): void;

    /** 创建动画对象。 */
    static createAnimator(options: AnimatorOptions): Animator;
  }
}
