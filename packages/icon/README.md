# Icon 图标

### 介绍

基于字体的图标集，可以通过 Icon 组件使用。

### 引入

在`pages/**/*.hml`中引入组件，详细介绍见[快速上手](#/quickstart#yin-ru-zu-jian)。

```html
<element name='van-icon' src='../../node_modules/oh-vant/lib/icon/icon.hml'></element>
```

## 代码演示

### 基础用法

`Icon`的`name`属性支持传入图标名称或图片链接。

```html
<van-icon name="close" />
<van-icon name="https://b.yzcdn.cn/vant/icon-demo-1126.png" />
```

### 提示信息

设置`dot`属性后，会在图标右上角展示一个小红点。设置`info`属性后，会在图标右上角展示相应的徽标。

```html
<van-icon name="chat" dot />
<van-icon name="chat" info="9" />
<van-icon name="chat" info="99+" />
```

### 图标颜色

设置`color`属性来控制图标颜色。

```html
<van-icon name="chat" color="red" />
```

### 图标大小

设置`size`属性来控制图标大小。

```html
<van-icon name="chat" size="50px" />
```

### 自定义图标

如果需要在现有 Icon 的基础上使用更多图标，可以通过 [openharmony-iconfont-cli](https://www.npmjs.com/package/openharmony-iconfont-cli) 工具生成自己的图标组件哦～

### 示例代码

- [传送门](https://gitee.com/vant-openharmony/example/tree/master/entry/src/main/js/default/pages/van_icon)
- [自定义图标](https://gitee.com/vant-openharmony/example/tree/master/entry/src/main/js/default/pages/custom_icon)

## API

### Props

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| name | 图标名称或图片链接 | _string_ | - | - |
| dot | 是否显示图标右上角小红点 | _boolean_ | `false` | - |
| badge | 图标右上角文字提示 | _string \| number_ | - | - |
| color | 图标颜色 | _string_ | - | - |
| size | 图标大小 | _number_ | `32` | - |

### Events

| 事件名     | 说明           | 参数 |
| ---------- | -------------- | ---- |
| @tap       | 点击图标时触发   | -    |
