# 快速上手

### 背景知识

使用 Vant Openharmony 前，请确保你已经学习过Openharmony官方的 [quick-start](https://gitee.com/openharmony/docs/tree/master/zh-cn/application-dev/quick-start) 和 [自定义组件介绍](https://developer.harmonyos.com/cn/docs/documentation/doc-references/js-components-custom-basic-usage-0000000000611781)。

## 安装

### 通过 npm 安装

在现有项目中使用 Vant 时，可以通过 `npm` 或 `yarn` 进行安装：

```bash
# 切换到 JS FA 目录
cd /path/to/entry/src/main/js/default

# 通过 npm 安装
npm i oh-vant -S

# 通过 yarn 安装
yarn add oh-vant
```

PS: 多FA应用开发可以通过 [lerna](https://lerna.js.org/) 进行开发

## 示例

### 示例工程

我们提供了丰富的[示例工程](https://gitee.com/vant-openharmony/example)。

## 引入组件

### 手动按需引入组件

在你需要使用组件的`hml`中引入组件。

```html
<element name='van-badge' src='../../node_modules/oh-vant/lib/badge/badge.hml'></element>
```
