// import { useChildren } from '../common/relation';
import { VantComponent, useChildren } from '@@/common';
import { TrivialInstance } from '@@/definitions';

export default VantComponent({
  name: 'checkbox-group',
  field: true,

  relation: useChildren('checkbox', function (target) {
    this.updateChild(target);
  }),

  props: {
    max: Number,
    value: Array,
    disabled: Boolean,
    direction: {
      type: String,
      default: 'vertical',
    },
  },
  lifetimes: {
    onInit() {
      this.$watch('value', 'updateChildren');
      this.$watch('disabled', 'updateChildren');
    },
  },
  methods: {
    updateChildren() {
      this.children$?.forEach((child) => this.updateChild(child));
    },

    updateChild(child: TrivialInstance) {
      const { value, disabled, direction } = this.data;
      child.setData$({
        value: value.indexOf(child.data.name) !== -1,
        parentDisabled: disabled,
        direction,
      });
    },
  },
});
