import { VantComponent } from '../common/component';

export default VantComponent({
  name: 'info',
  props: {
    dot: Boolean,
    info: null,
    customStyle: String,
  },
});
