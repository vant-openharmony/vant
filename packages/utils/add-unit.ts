const REGEXP = new RegExp('^-?d+(.d+)?$');

export function addUnit(value: string | number) {
  if (value == null) {
    return undefined;
  }

  return REGEXP.test('' + value) ? value + 'px' : value;
}
