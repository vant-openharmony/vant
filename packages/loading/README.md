# Loading 加载

### 介绍

加载图标，用于表示加载中的过渡状态。

### 引入

在`pages/**/*.hml`中引入组件，详细介绍见[快速上手](#/quickstart#yin-ru-zu-jian)。

```html
<element name='van-loading' src='../../node_modules/oh-vant/lib/loading/loading.hml'></element>
```

## 代码演示

### 加载类型

```html
<van-loading></van-loading>
<van-loading type="spinner" ></van-loading>
```

### 自定义颜色

```html
<van-loading color="#1989fa" ></van-loading>
<van-loading type="spinner" color="#1989fa" ></van-loading>
```

### 加载文案

```html
<van-loading size="{{ 24 }}"><text>加载中...</text></van-loading>
```

### 垂直排列

```html
<van-loading size="{{ 24 }}" vertical="true"><text>加载中...</text></van-loading>
```

## API

### Props

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| color | 颜色 | _string_ | `#c9c9c9` | - |
| type | 类型，可选值为 `spinner` | _string_ | `circular` | - |
| size | 加载图标大小，默认单位为 `px` | _string \| number_ | `30px` | - |
| text-size | 文字大小，默认单位为为 `px` | _string \| number_ | `14px` | 1.0.0 |
| vertical | 是否垂直排列图标和文字内容 | _boolean_ | `false` | 1.0.0 |

### Slots

| 名称 | 说明     |
| ---- | -------- |
| -    | 加载文案 |
