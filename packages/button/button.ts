import { VantComponent } from '@@/common';
import { CSSProperties } from '@@/definitions';

const colors: Record<string, string> = {
  default: '#323233',
  primary: '#07c160',
  info: '#1989fa',
  danger: '#ee0a24',
  warning: '#ff976a',
};

export default VantComponent({
  name: 'button',
  props: {
    formType: String,
    icon: String,
    color: String,
    plain: Boolean,
    block: Boolean,
    round: Boolean,
    square: Boolean,
    loading: Boolean,
    hairline: Boolean,
    disabled: Boolean,
    loadingText: String,
    customStyle: String,
    loadingType: {
      type: String,
      default: 'circular',
    },
    type: {
      type: String,
      default: 'default',
    },
    size: {
      type: String,
      default: 'normal',
    },
    loadingSize: {
      type: Number,
      default: 18,
    },
  },
  data() {
    return {
      hasClick: false,
    };
  },
  computed: {
    loadingColor() {
      if (this.plain) {
        if (this.color) return this.color;
        return colors[this.type];
      }
      return '#ffffff';
    },
    customizeStyle() {
      if (!this.color) {
        return undefined;
      }

      const properties: CSSProperties = {
        color: this.plain ? this.color : '#ffffff',
        backgroundColor: this.plain ? undefined : this.color,
      };

      // hide border when color is linear-gradient
      if (this.color.indexOf('gradient') !== -1) {
        properties.border = 'none';
      } else {
        properties.border = `1px solid ${this.color}`;
      }

      return properties;
    },
  },
  methods: {
    handClick() {
      if (this.disabled || this.loading) return;
      this.$emit('tap');
      this.setData$({ hasClick: !this.hasClick });
      setTimeout(() => {
        this.setData$({ hasClick: !this.hasClick });
      }, 300);
    },
  },
});
