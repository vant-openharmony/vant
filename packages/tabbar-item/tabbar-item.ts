import { VantComponent } from '../common/component';
import { useParent } from '../common/relation';

export default VantComponent({
  name: 'tabbar-item',
  props: {
    info: null,
    name: null,
    icon: String,
    dot: Boolean,
    iconPrefix: {
      type: String,
      value: 'van-icon',
    },
  },

  relation: useParent('tabbar'),

  data: {
    active: false,
    activeColor: '',
    inactiveColor: '',
  },

  methods: {
    onClick() {
      const { parent$ } = this;
      if (parent$) {
        const index = parent$.children?.indexOf(this);
        const active = this.name || index;

        if (active !== this.active) {
          parent$?.$emit('change', active);
        }
      }
      this.$emit('tap', { name: this.name });
    },

    updateFromParent() {
      const { parent$ } = this;
      if (!parent$) {
        return;
      }

      const index = parent$.children?.indexOf(this);
      const parentData = parent$.data;

      const active = (this.name || index) === parentData.active;
      const patch: Record<string, unknown> = {};

      if (active !== this.active) {
        patch.active = active;
      }
      if (parentData.activeColor !== this.activeColor) {
        patch.activeColor = parentData.activeColor;
      }
      if (parentData.inactiveColor !== this.inactiveColor) {
        patch.inactiveColor = parentData.inactiveColor;
      }

      if (Object.keys(patch).length > 0) {
        this.setData$(patch);
      }
    },
  },
});
