# Popup 弹出层

### 介绍

弹出层容器，用于展示弹窗、信息提示等内容。

### 引入

在`pages/**/*.hml`中引入组件，详细介绍见[快速上手](#/quickstart#yin-ru-zu-jian)。

```html
<element name='van-popup' src='../../node_modules/oh-vant/lib/popup/popup.hml'></element>
```

## 代码演示

### 基础用法

通过`visible`属性控制弹出层是否展示。

```html
<van-cell title="展示弹出层" is-link @tap="showPopup" ></van-cell>
<van-popup visible="{{ visible }}" @close="onClose">
  <div class="block">
    <text>内容</text>
  </div>
</van-popup>
```

```js
export default {
  data: {
    visible: false,
  },

  showPopup() {
    this.$set('visible', true);
  },

  onClose() {
    this.$set('visible', false);
  },
};
```

### 弹出位置

通过`position`属性设置弹出位置，默认居中弹出，可以设置为`top`、`bottom`、`left`、`right`。

```html
<van-popup
  visible="{{ visible }}"
  position="top"
  height="20%"
  @close="onClose"
></van-popup>
```

### 关闭图标

设置`closeable`属性后，会在弹出层的右上角显示关闭图标，并且可以通过`close-icon`属性自定义图标，使用`close-icon-position`属性可以自定义图标位置。

```html
<van-popup
  visible="{{ visible }}"
  closeable="true"
  position="bottom"
  height="20%"
  @close="onClose"
></van-popup>

<!-- 自定义图标 -->
<van-popup
  visible="{{ visible }}"
  closeable="true"
  close-icon="close"
  position="bottom"
  height="20%"
  @close="onClose"
></van-popup>

<!-- 图标位置 -->
<van-popup
  visible="{{ visible }}"
  closeable="true"
  close-icon-position="top-left"
  position="bottom"
  height="20%"
  @close="onClose"
></van-popup>
```

### 圆角弹窗

设置`round`属性后，弹窗会根据弹出位置添加不同的圆角样式。

```html
<van-popup
  visible="{{ visible }}"
  round="true"
  position="bottom"
  height="20%"
  @close="onClose"
></van-popup>
```

## API

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| visible | 是否显示弹出层 | _boolean_ | `false` |
| z-index | z-index 层级 | _number_ | `100` |
| overlay | 是否显示遮罩层 | _boolean_ | `true` |
| position | 弹出位置，可选值为 `top` `bottom` `right` `left` | _string_ | `center` |
| duration | 动画时长，单位为毫秒 | _number \| object_ | `300` |
| round | 是否显示圆角 | _boolean_ | `false` |
| close-on-click-overlay | 是否在点击遮罩层后关闭 | _boolean_ | `true` |
| closeable | 是否显示关闭图标 | _boolean_ | `false` |
| close-icon | 关闭图标名称或图片链接 | _string_ | `cross` |

### Events

| 事件名             | 说明               | 参数 |
| ------------------ | ---------------- | ---- |
| close              | 关闭弹出层时触发   | -    |
| click-overlay      | 点击遮罩层时触发   | -    |
