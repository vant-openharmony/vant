const { join } = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV;

const definitions = {
  production: {
    'process.env.PublicPath': '"/vant/"'
  },
  development: {
    'process.env.PublicPath': '"/"'
  }
}

module.exports = function () {
  if (process.env.BUILD_TARGET === 'package') {
    return {};
  }

  return {
    devtool: false,
    entry: {
      'site-mobile': ['./docs/site/entry'],
      'site-desktop': ['./docs/site/entry'],
    },
    resolve: {
      alias: {
        '@@': join(__dirname, 'packages'),
        '@demo': join(__dirname, 'docs', 'site'),
      },
    },
    plugins: [new webpack.DefinePlugin(definitions[NODE_ENV]), new CopyWebpackPlugin({ patterns: ['static'] })],
  };
};
