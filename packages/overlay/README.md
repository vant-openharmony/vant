# Overlay 遮罩层

### 介绍

创建一个遮罩层，用于强调特定的页面元素，并阻止用户进行其他操作。

### 引入

在`pages/**/*.hml`中引入组件，详细介绍见[快速上手](#/quickstart#yin-ru-zu-jian)。

```html
<element name='van-overlay' src='../../node_modules/oh-vant/lib/overlay/overlay.hml'></element>
```

## 代码演示

### 基础用法

```html
<van-button type="primary" @tap="onClickShow"><text>显示遮罩层</text></van-button>
<van-overlay visible="{{ visible }}" @tap="onClickHide" ></van-overlay>
```

```js
export default {
  data: {
    visible: false,
  },

  onClickShow() {
    this.$set('visible', true);
  },

  onClickHide() {
    this.$set('visible', false);
  },
};
```

### 嵌入内容

通过默认插槽可以在遮罩层上嵌入任意内容。

```html
<van-button type="primary" @tap="onClickShow"><text>嵌入内容</text></van-button>
<van-overlay visible="{{ visible }}" @tap="onClickHide">
  <div class="wrapper">
    <div class="block" @click="noop" ></div>
  </div>
</van-overlay>
```

```js
Page({
  data: {
    visible: false,
  },

  onClickShow() {
    this.$set('visible', true);
  },

    onClickHide() {
    this.$set('visible', false);
  },

  noop() {},
});
```

```css
.wrapper {
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
}

.block {
  width: 120px;
  height: 120px;
  background-color: #fff;
}
```

### Props

| 参数 | 说明 | 类型 | 默认值 |
| --- | --- | --- | --- |
| visible | 是否展示遮罩层 | _boolean_ | `false` |
| z-index | z-index 层级 | _string \| number_ | `1` |
| duration | 动画时长，单位秒 | _string \| number_ | `0.3` |

### Events

| 事件名     | 说明       | 回调参数 |
| ---------- | ---------- | -------- |
| @tap | 点击时触发 | -        |

### Slots

| 名称 | 说明                               |
| ---- | ---------------------------------- |
| -    | 默认插槽，用于在遮罩层上方嵌入内容 |
