import omit from 'lodash/omit';
import isFunction from 'lodash/isFunction';
import { createNamespace } from '@@/utils';
import {
  TrivialInstance,
  ComponentOptions,
  DataOption,
  PropertyOption,
  MethodOption,
  ZapOption,
  IAnyObject,
  IComponentLifetime,
} from '@@/definitions';
import { global } from './global';
import { Behavior } from './behavior';

function VantComponent<
  TData extends DataOption,
  TProperty extends PropertyOption,
  TMethod extends MethodOption,
  TIsField extends boolean = false,
  TCustomInstanceProperty extends IAnyObject = {}
>(
  vantOptions: ComponentOptions<
    TData,
    TProperty,
    TMethod,
    TCustomInstanceProperty,
    TIsField
  >
): Record<string, any> {
  const { name, field, relation } = vantOptions;
  const [, bem] = createNamespace(name);
  // let ancestor: TrivialInstance | undefined = undefined;
  // const descendants: TrivialInstance[] = [];

  const zapOptions: ZapOption = {
    behaviors: [Behavior({ data: { bem } })],
    relations: {},
  };

  if (field) {
    zapOptions.behaviors.push(
      Behavior({ props: { name: String, value: null } })
    );
  }
  if (relation) {
    zapOptions.relations = relation.relations;
    zapOptions.behaviors.push(relation.mixin);
  }

  // behaviors 极简版，不具备父子关系
  // 规则参考： https://developers.weixin.qq.com/miniprogram/dev/framework/custom-component/behaviors.html
  const behaviors = zapOptions.behaviors.map((k) => global.behaviors[k]);
  behaviors.push(vantOptions);

  const behaviorLifetimes = behaviors.map(({ lifetimes }) => lifetimes || {});
  const behaviorMethods = behaviors.map(({ methods }) => methods);
  const behaviorProps = behaviors.map(({ props }) => props);
  const behaviorDatas = behaviors.map(({ data }) =>
    isFunction(data) ? data : () => data!
  );
  const lifetimeKeys: Array<keyof IComponentLifetime> = [
    'onInit',
    'onAttached',
    'onDestroy',
    'onDetached',
    'onLayoutReady',
    'onPageShow',
    'onPageHide',
  ];
  const lifetimes: Partial<IComponentLifetime> = {};
  lifetimeKeys.forEach((k) => {
    lifetimes[k] = async function () {
      for (const { [k]: fn } of behaviorLifetimes) {
        await fn?.();
      }
    };
  });

  // relations 极简版
  // 规则参考：https://developers.weixin.qq.com/miniprogram/dev/framework/custom-component/relations.html
  // const relationLifetimes: Partial<IComponentLifetime>[] = [];
  // Object.keys(zapOptions.relations).forEach((k) => {
  //   const lifetimes: Partial<IComponentLifetime> = {};
  //   const { type } = get(zapOptions.relations, k);
  //   relationLifetimes.push(lifetimes);
  //   switch (type) {
  //     case 'ancestor':
  //       lifetimes.onInit = function () {
  //         global.event$.many(({ uniqueKey, action, params }) => {
  //           if (uniqueKey === k && action === 'relation') {
  //             if (!ancestor) ancestor = params.instance;
  //           }
  //         });
  //       };
  //       break;
  //     case 'descendant':
  //       lifetimes.onInit = function () {
  //         global.event$.many(({ uniqueKey, action, params }) => {
  //           if (uniqueKey === k && action === 'relation') {
  //             descendants.push(params.instance);
  //           }
  //         });
  //       };
  //       break;
  //   }
  // });
  const newLifetimes: Partial<IComponentLifetime> = {
    ...lifetimes,
    // onInit() {
    //   console.log(name + '__onInit');
    //   lifetimes.onInit?.();
    // },
    // async onDetached() {
    //   await lifetimes.onDetached?.();
    // },
    // async onLayoutReady() {
    //   console.log(name + '__onLayoutReady');
    //   global.event$.emit({
    //     uniqueKey: name,
    //     action: 'relation',
    //     params: { instance: this },
    //   });
    //   await lifetimes.onLayoutReady?.();
    // },
  };
  const newObj: Record<string, any> = {
    ...omit(vantOptions, ['data', 'props', 'lifetimes', 'methods']),
    // ...newLifetimes,
    ...vantOptions.lifetimes,
    ...behaviorMethods.reduce((p, c) => ({ ...p, ...c }), {}),
    props: behaviorProps.reduce((p, c) => ({ ...p, ...c }), {}),
    data() {
      return behaviorDatas.reduce((p, c) => ({ ...p, ...c?.() }), {});
    },
    setData$(data: IAnyObject) {
      const that = this;
      Object.keys(data).forEach((k) => {
        that.$set(k, data[k]);
      });
    },
    getRelationNodes(path: string) {
      const nodes: TrivialInstance[] = [];
      return nodes;
    },
  };

  return newObj;
}

export { VantComponent };
