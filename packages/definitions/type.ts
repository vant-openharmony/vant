type PropConstructor<T = any> =
  | {
      new (...args: any[]): T & {};
    }
  | {
      (): T;
    }
  | PropMethod<T>;
declare type PropMethod<T, TConstructor = any> = [T] extends [
  ((...args: any) => any) | undefined
]
  ? {
      new (): TConstructor;
      (): T;
      readonly prototype: TConstructor;
    }
  : never;

export type PropType<T> = PropConstructor<T> | PropConstructor<T>[];

export interface EEValue {
  uniqueKey: string | symbol;
  action?: string;
  params?: any;
  [propName: string]: any;
}
