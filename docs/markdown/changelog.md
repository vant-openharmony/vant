# Changelog

<a name="0.1.6"></a>
## 0.1.6 (2022-05-05)

### Changed

- 🎨 优化nav-bar样式 [d38a912]

### Fixed

- 🐛 [cell]解决字体样式失效问题 [6396d2b]

### Miscellaneous

-  0.1.6 [e0f32cf]
-  COMMIT [d62a921]
-  changelog [7bb3389]


<a name="0.1.5"></a>
## 0.1.5 (2022-02-22)

### Changed

- 🎨 nav-bar [4c53a8f]

### Miscellaneous

-  0.1.5 [81721b7]
-  changelog [49da284]


<a name="0.1.4"></a>
## 0.1.4 (2022-02-21)

### Added

- ✨ nav-bar [34af395]
- ✨ tabbar [1c61fc1]

### Removed

- 🔥 删除lodash [bdda69f]

### Miscellaneous

-  0.1.4 [caafeff]
-  changelog [02eefe2]


<a name="0.1.3"></a>
## 0.1.3 (2021-11-15)

### Miscellaneous

-  v0.1.3 [b087c6c] (by sengmitnick)


<a name="0.1.2"></a>
## 0.1.2 (2021-11-15)

### Fixed

- 🐛 A data binding parsing error occurred:show [95679b1] (by sengmitnick)

### Miscellaneous

-  v0.1.2 [ad0db7b] (by sengmitnick)
-  changelog [feff589] (by sengmitnick)


<a name="0.1.1"></a>
## 0.1.1 (2021-11-15)

### Miscellaneous

-  v0.1.1 [fce4d4c] (by sengmitnick)
-  自动更新 [54799cd] (by sengmitnick)


<a name="0.1.0"></a>
## 0.1.0 (2021-09-19)

### Added

- ✨ popup [f3ac6ac] (by sengmitnick)
- ✨ add loading [e918f25] (by sengmitnick)
- ✅ cell [08d5d24] (by sengmitnick)
- ✨ add demo gif [da65a7e] (by sengmitnick)
- ✨ add relations and behaviors [c660c49] (by sengmitnick)

### Changed

- ⚡ add mov [f3fa90e] (by sengmitnick)
- 🎨 overlay done [838cf87] (by sengmitnick)
- 🎨 button done [cb4d98b] (by sengmitnick)
- 🎨 优化生产和dev环境publicPath不一致的问题 [9babd58] (by sengmitnick)
- 🎨 调整checkbox [0b2edf0] (by sengmitnick)

### Removed

- 🔥 删除无效代码 [8b8bef7] (by sengmitnick)
- 🔥 relations功能搁置 [1637c66] (by sengmitnick)

### Fixed

- 🐛 自定义颜色影响到plain功能了 [516e08d] (by sengmitnick)
- 🐛 fix button click [bae9978] (by sengmitnick)
- 🐛 fix checkbox js不生效 [b4fd920] (by sengmitnick)
- 🐛 fix size 无效 [48eb63d] (by sengmitnick)
- 🐛 fix icon padding [fd29b0e] (by sengmitnick)
- 🐛 fix 线上静态资源缺失 [fbc8a90] (by sengmitnick)

### Miscellaneous

-  v0.1.0 [0eeaebf] (by sengmitnick)
-  自动更新 [f2ee232] (by sengmitnick)
-  COMMIT [ddbd883] (by sengmitnick)
- 🚧 popup开发中 [5aa0b8b] (by sengmitnick)
- 📝 更新overlay [e824767] (by sengmitnick)
- 📝 更新Cell文档 [5b4d336] (by sengmitnick)
- 📝 cell [abbcf7b] (by sengmitnick)
- 📝 update home todo [ca4670b] (by sengmitnick)
- 📝 update README [4712c39] (by sengmitnick)
- 📝 update README [418ccf6] (by sengmitnick)
- 📝 update README [9103e00] (by sengmitnick)
- 📝 update icon custom_icon [86c4fc0] (by sengmitnick)
-  Merge branch &#x27;master&#x27; of https://gitee.com/vant-openharmony/vant [889e08b] (by sengmitnick)
- 📝 add todo [f0c2818] (by sengmitnick)
- 📝 删除未实现的组件文档 [b8e7882] (by sengmitnick)


<a name="0.0.11"></a>
## 0.0.11 (2021-08-31)

### Added

- ✨ van-preview add accept [4a3a487] (by sengmitnick)
- ✨ add checkbox [3ed248d] (by sengmitnick)
- ✨ add EventEmitter [4c07c5a] (by sengmitnick)
- ✨ 完善dev script [b7b6fdf] (by sengmitnick)
- ✨ add @hd [4c2ca04] (by sengmitnick)
- ✨ add button [f6a5ec5] (by sengmitnick)
- ✨ add loading [f7f9e62] (by sengmitnick)
- ✨ add cell and cell-group [20faf84] (by sengmitnick)
- ✨ add  TouchEvent 声明 [de2d53b] (by sengmitnick)
- ✨ VantComponent props 支持数组类型 [d199a27] (by sengmitnick)
- ✨ add  PropType 声明 [cf1583e] (by sengmitnick)
- ✨ add badge [cbffb43] (by sengmitnick)
- ✨ 添加全局组件van-preview并应用 [e2155d4] (by sengmitnick)
- ✨ 文档Demo功能验证完成 [9453ea3] (by sengmitnick)
- ✨ 初步完善文档功能 [1a4f489] (by sengmitnick)
- ✨ 继续完善VantComponent [5647434] (by sengmitnick)
- ✨ 初步完善VantComponent的ts声明 [09c0ec9] (by sengmitnick)

### Changed

- ⬆️ openharmony-iconfont-cli [e0d2727] (by sengmitnick)
- 🎨 update dev scripts [6d1de83] (by sengmitnick)
- 🎨 badge 支持offset、color [6d9074a] (by sengmitnick)
- ♻️ update build scripts [f95062a] (by sengmitnick)
- ♻️ 优化代码引入功能 [f6fad13] (by sengmitnick)

### Fixed

- 🐛 fix element src [1c0f949] (by sengmitnick)
- 🐛 fix badge slot [7b4be9b] (by sengmitnick)

### Miscellaneous

-  v0.0.11 [4b21e47] (by sengmitnick)
- 🚧 修复导致不能编译的代码 [d010ddb] (by sengmitnick)
- 📝 (icon): 删除未实现的代码演示和API [fc57f38] (by sengmitnick)
- ⚰️ (icon): 删除不再需要的代码 [f90b884] (by sengmitnick)
- 📝 (badge): 删除未实现的代码演示，添加TODO [3def8b8] (by sengmitnick)
-  sparkles: add checkbox-group [c083842] (by sengmitnick)
- 📝 update badge [1a2989e] (by sengmitnick)
- 📝 update 快速上手 [6580289] (by sengmitnick)
- 🚧 修复导致不能编译的代码 [ac06252] (by sengmitnick)
- 💩 优化脚本 [71d42a6] (by sengmitnick)
- 🚧 完善了badge组件 [3705ee8] (by sengmitnick)
- 📝 更新官网配置 [9c8ee87] (by sengmitnick)
- 📝 更新已有组件引入一块说明 [8453507] (by sengmitnick)
- 📝 完善icon的demo文档 [1073489] (by sengmitnick)
-  COMMIT [030f919] (by sengmitnick)


<a name="0.0.10"></a>
## 0.0.10 (2021-08-22)

### Miscellaneous

-  v0.0.10 [35c39c5]
-  COMMIT [7b76f61]
- 🤖 update icon [2c9f639]


<a name="0.0.9"></a>
## 0.0.9 (2021-08-22)

### Miscellaneous

-  v0.0.9 [05fbe90]


<a name="0.0.8"></a>
## 0.0.8 (2021-08-22)

### Changed

- ⬆️ openharmony-iconfont-cli [cef960e]

### Miscellaneous

-  v0.0.8 [81195f1]
- 🤖 update icon [b35bac7]


<a name="0.0.7"></a>
## 0.0.7 (2021-08-20)

### Miscellaneous

-  v0.0.7 [18daf80]
-  COMMIT [32e6c2b]


<a name="0.0.6"></a>
## 0.0.6 (2021-08-20)

### Miscellaneous

-  v0.0.6 [865fc3c]
-  COMMIT [aaf02e5]


<a name="0.0.5"></a>
## 0.0.5 (2021-08-20)

### Changed

- ♻️ Refactoring the packaging function [87ae6f5]

### Breaking changes

- 💥 Further improve VantComponent [5933a3f]
- 💥 src/* &#x3D;&gt; packages/* [ec3f5dc]
- 💥 add link [e416769]
- 💥 add openharmony-iconfont-cli [f1e8acc]

### Miscellaneous

-  release: 0.0.5 [68f81fb]
-  COMMIT [713091d]
- ⚰️  [f715e3a]
- 🤖 icon 重新生成 [f3a1df3]
- 📝 Document function is being implemented [db4b4a9]


<a name="0.0.3"></a>
## 0.0.3 (2021-08-19)

### Added

- ✨ icon add size [e0ea95f]

### Miscellaneous

-  v0.0.3 [b15efd6]


<a name="0.0.2"></a>
## 0.0.2 (2021-08-19)

### Added

- ✨ add icon [9ed2986]

### Miscellaneous

-  v0.0.2 [aeecb41]
-  COMMIT [039b7a3]


<a name="0.0.1"></a>
## 0.0.1 (2021-08-18)

### Breaking changes

- 💥 rename pkgName and description [7a74518]

### Miscellaneous

-  v0.0.1 [8f3ee3d]


<a name="1.0.3"></a>
## 1.0.3 (2021-08-18)

### Miscellaneous

-  v1.0.3 [ab83d38]
- 🚧 Verify how to apply the npm package [0362df8]


<a name="1.0.2"></a>
## 1.0.2 (2021-08-18)

### Changed

- 🎨 Remove unnecessary dependencies [1e0a1f1]

### Miscellaneous

-  v1.0.2 [73e31e3]


<a name="1.0.1"></a>
## 1.0.1 (2021-08-18)

### Added

- 🎉 new pro [1a598a1]

### Breaking changes

- 💥 add np [58340d7]
- 💥 father &#x3D;&gt; umi-tools [a45afc3]

### Miscellaneous

-  v1.0.1 [d5db571]
- 🚧 add lib to gitignore [164eb86]
- ⚰️ del lib [0ae52f9]
-  Initial commit [3674053]


<a name="0.0.10"></a>
## 0.0.10 (2021-08-22)

### Miscellaneous

-  v0.0.10 [35c39c5]
-  COMMIT [7b76f61]
- 🤖 update icon [2c9f639]


<a name="0.0.9"></a>
## 0.0.9 (2021-08-22)

### Miscellaneous

-  v0.0.9 [05fbe90]


<a name="0.0.8"></a>
## 0.0.8 (2021-08-22)

### Changed

- ⬆️ openharmony-iconfont-cli [cef960e]

### Miscellaneous

-  v0.0.8 [81195f1]
- 🤖 update icon [b35bac7]


<a name="0.0.7"></a>
## 0.0.7 (2021-08-20)

### Miscellaneous

-  v0.0.7 [18daf80]
-  COMMIT [32e6c2b]


<a name="0.0.6"></a>
## 0.0.6 (2021-08-20)

### Miscellaneous

-  v0.0.6 [865fc3c]
-  COMMIT [aaf02e5]


<a name="0.0.5"></a>
## 0.0.5 (2021-08-20)

### Changed

- ♻️ Refactoring the packaging function [87ae6f5]

### Breaking changes

- 💥 Further improve VantComponent [5933a3f]
- 💥 src/* &#x3D;&gt; packages/* [ec3f5dc]
- 💥 add link [e416769]
- 💥 add openharmony-iconfont-cli [f1e8acc]

### Miscellaneous

-  release: 0.0.5 [68f81fb]
-  COMMIT [713091d]
- ⚰️  [f715e3a]
- 🤖 icon 重新生成 [f3a1df3]
- 📝 Document function is being implemented [db4b4a9]


<a name="0.0.3"></a>
## 0.0.3 (2021-08-19)

### Added

- ✨ icon add size [e0ea95f]

### Miscellaneous

-  v0.0.3 [b15efd6]


<a name="0.0.2"></a>
## 0.0.2 (2021-08-19)

### Added

- ✨ add icon [9ed2986]

### Miscellaneous

-  v0.0.2 [aeecb41]
-  COMMIT [039b7a3]


<a name="0.0.1"></a>
## 0.0.1 (2021-08-18)

### Breaking changes

- 💥 rename pkgName and description [7a74518]

### Miscellaneous

-  v0.0.1 [8f3ee3d]


