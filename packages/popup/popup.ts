import Animator, { AnimatorOptions } from '@ohos.animator';
import { VantComponent } from '@@/common';

const animatorOptionMap: Record<string, AnimatorOptions> = {
  center: { begin: 0, end: 1 },
  top: { begin: 0, end: -50 },
  right: { begin: 100, end: 0 },
  bottom: { begin: 0, end: -50 },
  left: { begin: -100, end: 0 },
};

export default VantComponent({
  name: 'popup',

  props: {
    visible: Boolean,
    round: Boolean,
    closeable: Boolean,
    customStyle: String,
    overlayStyle: String,
    width: String,
    height: String,
    zIndex: {
      type: Number,
      default: 100,
    },
    duration: {
      type: Number,
      default: 300,
    },
    overlay: {
      type: Boolean,
      default: true,
    },
    closeIcon: {
      type: String,
      default: 'cross',
    },
    closeIconPosition: {
      type: String,
      default: 'top-right',
    },
    closeOnClickOverlay: {
      type: Boolean,
      default: true,
    },
    position: {
      type: String,
      default: 'center',
      // observer: 'observeClass',
    },
    lockScroll: {
      type: Boolean,
      default: true,
    },
  },

  data() {
    return {
      display: false,
      opacity: 0,
      transform: 'translate3d(-50%, -50%, 0)',
      animatorOptions: {} as AnimatorOptions,
      animator: null as unknown as Animator,
    };
  },

  lifetimes: {
    onInit() {
      this.animatorOptions = {
        duration: this.duration,
        easing: 'ease',
        // fill: 'forwards',
        iterations: 1,
        begin: 0.0,
        end: 100.0,
      };
      this.animator = Animator.createAnimator(this.animatorOptions);
      var _this = this;
      this.animator.onframe = function (timestamp) {
        const { position } = _this;
        switch (position) {
          case 'center':
            _this.upData({
              opacity: timestamp,
              translateX: -50,
              translateY: -50,
            });
            break;
          case 'top': // translateX 0 => -50 translateY => -100 => -50
            _this.upData({ opacity: 1, translateX: -50, translateY: -50 });
            break;
          case 'right': // translateX 100 => 0
            _this.upData({
              opacity: 1,
              translateX: timestamp,
              translateY: -50,
            });
            break;
          case 'bottom': // translateX 0 => -50 translateY => 100 => -50
            _this.upData({ opacity: 1, translateX: -50, translateY: -50 });
            break;
          case 'left': // translateX -100 => 0
            _this.upData({
              opacity: 1,
              translateX: timestamp,
              translateY: -50,
            });
            break;
        }
      };
      this.animator.onfinish = function () {
        if (!_this.visible) _this.setData$({ opacity: 0, display: false });
      };

      // if (this.visible === true) {
      //   this.observeShow(true, false);
      // }

      this.$watch('visible', 'observeShow');
    },
  },

  methods: {
    handClick(event) {
      this.$emit('tap');
    },

    onClickCloseIcon() {
      this.$emit('close');
    },

    onClickOverlay() {
      this.$emit('click-overlay');

      if (this.closeOnClickOverlay) {
        this.$emit('close');
      }
    },

    num2str(translate: number | string) {
      if (translate === '0.000000') return '0';
      return translate + '%';
    },

    upData({
      opacity,
      translateX,
      translateY,
    }: {
      opacity: number;
      translateX: number;
      translateY: number;
    }) {
      this.setData$({
        opacity,
        transform: `translate3d(${this.num2str(translateX)},${this.num2str(
          translateY
        )}, 0)`,
      });
      console.log(
        ` translate3d(${this.num2str(translateX)},${this.num2str(
          translateY
        )}, 0)`
      );
    },

    observeShow(value: boolean, old: boolean) {
      if (value === old) {
        return;
      }

      value ? this.enter() : this.leave();
    },

    enter() {
      this.setData$({ display: true });
      const { begin, end } = animatorOptionMap[this.position];
      this.animatorOptions.begin = begin;
      this.animatorOptions.end = end;
      this.animator.update(this.animatorOptions);
      this.animator.play();
    },
    leave() {
      const { begin, end } = animatorOptionMap[this.position];
      this.animatorOptions.begin = end;
      this.animatorOptions.end = begin;
      this.animator.update(this.animatorOptions);
      this.animator.play();
    },
  },
});
