import { VantComponent } from '@@/common';

export default VantComponent({
  name: 'cell',
  props: {
    title: null,
    value: null,
    icon: String,
    iconColor: {
      type: String,
      default: '#969799',
    },
    tapParams: null,
    size: String,
    label: String,
    center: Boolean,
    isLink: Boolean,
    required: Boolean,
    clickable: Boolean,
    titleWidth: String,
    customStyle: String,
    arrowDirection: String,
    useLabelSlot: Boolean,
    border: {
      type: Boolean,
      default: true,
    },
    titleStyle: String,
  },
  methods: {
    onClick() {
      this.$emit('tap', { data: this.tapParams });
    },
  },
});
