type Subscription<T> = (val: T) => void;
export interface EEInstance {
  off(): void;
}

export class EventEmitter<T> {
  private subscriptions = new Set<Subscription<T>>();

  emit = (val: T) => {
    for (const subscription of this.subscriptions) {
      subscription(val);
    }
  };

  many = (callback: Subscription<T>): EEInstance => {
    function subscription(val: T) {
      if (callback) {
        callback(val);
      }
    }
    this.subscriptions.add(subscription);
    return {
      off: () => {
        this.subscriptions.delete(subscription);
      },
    };
  };
}
