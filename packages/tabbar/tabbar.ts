import { VantComponent } from '../common/component';
import { useChildren } from '../common/relation';
import { TrivialInstance } from '@@/definitions';

export default VantComponent({
  name: 'tabbar',
  relation: useChildren('tabbar-item', function () {
    this.updateChildren();
  }),

  props: {
    active: {
      type: null,
      // observer: 'updateChildren',
    },
    activeColor: {
      type: String,
      // observer: 'updateChildren',
    },
    inactiveColor: {
      type: String,
      // observer: 'updateChildren',
    },
    fixed: {
      type: Boolean,
      value: true,
      // observer: 'setHeight',
    },
    placeholder: {
      type: Boolean,
      // observer: 'setHeight',
    },
    border: {
      type: Boolean,
      default: true,
    },
    zIndex: {
      type: Number,
      default: 1,
    },
    safeAreaInsetBottom: {
      type: Boolean,
      default: true,
    },
  },

  data: {
    height: 50,
  },

  methods: {
    updateChildren() {
      const { children$ } = this;
      if (!Array.isArray(children$) || !children$?.length) {
        return;
      }

      children$?.forEach((child: TrivialInstance) => child.updateFromParent());
    },

    setHeight() {
      if (!this.fixed || !this.placeholder) {
        return;
      }

      // wx.nextTick(() => {
      //   getRect(this, '.van-tabbar').then((res) => {
      //     this.setData({ height: res.height });
      //   });
      // });
    },
  },
});
