var gulp = require('gulp');
// var del = require('del');
var less = require('gulp-less');
var LessNpmImport = require('less-plugin-npm-import');

var theme = require('./theme');

// gulp.task('clean', () => del(['dist/**/*']));

gulp.task('less', () =>
  gulp
    .src('src/**/*.less')
    .pipe(
      less({
        plugins: [new LessNpmImport({ prefix: '~' })],
        javascriptEnabled: true,
        modifyVars: theme,
      }),
    )
    .pipe(gulp.dest('lib/components/'))
    .pipe(gulp.dest('es/components/')),
);

gulp.task('less_style', () =>
  gulp
    .src('src/components/_style/*.less')
    .pipe(
      less({
        plugins: [new LessNpmImport({ prefix: '~' })],
        javascriptEnabled: true,
        modifyVars: theme,
      }),
    )
    .pipe(gulp.dest('dist/')),
);

gulp.task('default', gulp.series('clean', 'less', 'less_style'));
