#!/usr/bin/env sh
set -e

# build
npm run build
./node_modules/.bin/np --no-cleanup --yolo --no-publish

npm publish --access=public

# changelog
npm run changelog
