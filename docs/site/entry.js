import Preview from './Preview';

const { app } = window;
if (app) {
  app.component(Preview.name, Preview);
  app.config.globalProperties.$publicPath = process.env.PublicPath;
}
