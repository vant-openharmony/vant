#!/usr/bin/env sh

mkdir .tmp
mkdir .tmp/demo
cp packages/icon/README.md .tmp/README.md
cp packages/icon/index.vue .tmp/index.vue
cp packages/icon/demo/* .tmp/demo
rm -rf packages/icon
npx iconfont-oh
npx iconfont-oh --config loading-iconfont.json
cp -rf .tmp/* packages/icon
rm -rf .tmp
