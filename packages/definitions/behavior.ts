import {
  Instance,
  Data,
  Property,
  Method,
  DataOption,
  PropertyOption,
  MethodOption,
  OtherOption,
  Lifetimes,
} from './component';

export type BehaviorOptions<
  TData extends DataOption,
  TProperty extends PropertyOption,
  TMethod extends MethodOption
> = Partial<Data<TData>> &
  Partial<Property<TProperty>> &
  Partial<Method<TMethod>> &
  Partial<Lifetimes> &
  Partial<OtherOption> &
  ThisType<Instance<TData, TProperty, TMethod, false, {}>>;
