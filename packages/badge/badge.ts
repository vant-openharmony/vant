import { VantComponent } from '@@/common';
import { truthProp, isDef, isNumeric, addUnit } from '@@/utils';
import { CSSProperties } from '@@/definitions';

export default VantComponent({
  name: 'badge',
  props: {
    dot: Boolean,
    max: [Number, String],
    color: String,
    offset: Array,
    content: [Number, String],
    showZero: truthProp,
  },
  data() {
    return {};
  },
  computed: {
    fixed() {
      return !this.dot;
    },
    style() {
      const style: CSSProperties = {
        backgroundColor: this.color,
      };

      if (this.offset) {
        const [x, y] = this.offset;
        style.top = addUnit(y);

        if (typeof x === 'number') {
          style.right = addUnit(-x);
        } else {
          style.right = x.startsWith('-') ? x.replace('-', '') : `-${x}`;
        }
      }

      return style;
    },
    children() {
      const { dot, max, content } = this;
      if (!dot && this.hasContent()) {
        // if (this.$slots.content) {
        //   return this.$slots.content();
        // }

        if (isDef(max) && isNumeric(content!) && +content > max) {
          return `${max}+`;
        }

        return content;
      }
    },
  },
  methods: {
    hasContent() {
      // if (this.$slots.content) {
      //   return true;
      // }
      const { content, showZero } = this;
      return isDef(content) && content !== '' && (showZero || content !== 0);
    },
  },
});
