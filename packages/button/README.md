# Button 按钮

### 介绍

按钮用于触发一个操作，如提交表单。

### 引入

在`pages/**/*.hml`中引入组件，详细介绍见[快速上手](#/quickstart#yin-ru-zu-jian)。

```html
<element name='van-button' src='../../node_modules/oh-vant/lib/button/button.hml'></element>
```

## 代码演示

### 按钮类型

支持`default`、`primary`、`info`、`warning`、`danger`五种类型，默认为`default`。

```html
<van-button type="default"><text>默认按钮</text></van-button>
<van-button type="primary"><text>主要按钮</text></van-button>
<van-button type="info"><text>信息按钮</text></van-button>
<van-button type="warning"><text>警告按钮</text></van-button>
<van-button type="danger"><text>危险按钮</text></van-button>
```

### 朴素按钮

通过`plain`属性将按钮设置为朴素按钮，朴素按钮的文字为按钮颜色，背景为白色。

```html
<van-button plain="true" type="primary"><text>朴素按钮</text></van-button>
<van-button plain="true" type="info"><text>朴素按钮</text></van-button>
```

### 禁用状态

通过`disabled`属性来禁用按钮，此时按钮的`bind:click`事件不会触发。

```html
<van-button disabled="true" type="primary"><text>禁用状态</text></van-button>
<van-button disabled="true" type="info"><text>禁用状态</text></van-button>
```

### 加载状态

```html
<van-button loading="true" type="primary" ></van-button>
<van-button loading="true" type="primary" loading-type="spinner" ></van-button>
<van-button loading="true" type="info" loading-text="加载中..." ></van-button>
```

### 按钮形状

```html
<van-button square="true" type="primary"><text>方形按钮</text></van-button>
<van-button round="true" type="info"><text>圆形按钮</text></van-button>
```

### 图标按钮

通过`icon`属性设置按钮图标，支持 Icon 组件里的所有图标，也可以传入图标 URL。

```html
<van-button icon="star-o" type="primary" ></van-button>
<van-button icon="star-o" type="primary"><text>按钮</text></van-button>
<van-button icon="https://img.yzcdn.cn/vant/logo.png" type="info">
  <text>按钮</text>
</van-button>
```

### 按钮尺寸

支持`large`、`normal`、`small`、`mini`四种尺寸，默认为`normal`。

```html
<van-button type="primary" size="large"><text>大号按钮</text></van-button>
<van-button type="primary" size="normal"><text>普通按钮</text></van-button>
<van-button type="primary" size="small"><text>小型按钮</text></van-button>
<van-button type="primary" size="mini"><text>迷你按钮</text></van-button>
```

### 块级元素

通过`block`属性可以将按钮的元素类型设置为块级元素。

```html
<van-button type="primary" block="true"><text>块级元素</text></van-button>
```

<!-- ### 自定义颜色

通过`color`属性可以自定义按钮的颜色。

```html
<van-button color="#7232dd"><text>单色按钮</text></van-button>
<van-button color="#7232dd" plain="true"><text>单色按钮</text></van-button>
<van-button color="linear-gradient(to right, #4bb0ff, #6149f6)">
  <text>渐变色按钮</text>
</van-button>
``` -->

## API

### Props

| 参数 | 说明 | 类型 | 默认值 | 版本 |
| --- | --- | --- | --- | --- |
| id | 标识符 | _string_ | - | - |
| type | 按钮类型，可选值为 `primary` `info` `warning` `danger` | _string_ | `default` | - |
| size | 按钮尺寸，可选值为 `normal` `large` `small` `mini` | _string_ | `normal` | - |
| icon | 左侧图标名称或图片链接，可选值见 [Icon 组件](#/icon) | _string_ | - | - |
| plain | 是否为朴素按钮 | _boolean_ | `false` | - |
| block | 是否为块级元素 | _boolean_ | `false` | - |
| round | 是否为圆形按钮 | _boolean_ | `false` | - |
| square | 是否为方形按钮 | _boolean_ | `false` | - |
| disabled | 是否禁用按钮 | _boolean_ | `false` | - |
| hairline | 是否使用 0.5px 边框 | _boolean_ | `false` | - |
| loading | 是否显示为加载状态 | _boolean_ | `false` | - |
| loading-text | 加载状态提示文字 | _string_ | - | - |
| loading-type | 加载状态图标类型，可选值为 `spinner` | _string_ | `circular` | - |
| loading-size | 加载图标大小 | _string_ | `20px` | - |

### Events

| 事件名 | 说明 | 参数 |
| --- | --- | --- |
| tap | 点击按钮，且按钮状态不为加载或禁用时触发 | - |
