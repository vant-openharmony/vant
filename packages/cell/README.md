# Cell 单元格

### 介绍

单元格为列表中的单个展示项。

### 引入

在`pages/**/*.hml`中引入组件，详细介绍见[快速上手](#/quickstart#yin-ru-zu-jian)。

```html
<element name='van-cell' src='../../node_modules/oh-vant/lib/cell/cell.hml'></element>
<element name='van-cell-group' src='../../node_modules/oh-vant/lib/cell-group/cell-group.hml'></element>
```

## 代码演示

### 基础用法

`Cell`可以单独使用，也可以与`CellGroup`搭配使用。`CellGroup`可以为`Cell`提供上下外边框。

```html
<van-cell-group>
  <van-cell title="单元格" value="内容"></van-cell>
  <van-cell title="单元格" value="内容" label="描述信息" border="{{ false }}"></van-cell>
</van-cell-group>
```

### 卡片风格

通过 `CellGroup` 的 `inset` 属性，可以将单元格转换为圆角卡片风格（从 1.7.2 版本开始支持）。

```html
<van-cell-group inset="true">
  <van-cell title="单元格" value="内容"></van-cell>
  <van-cell title="单元格" value="内容" label="描述信息"></van-cell>
</van-cell-group>
```

### 单元格大小

通过`size`属性可以控制单元格的大小。

```html
<van-cell title="单元格" value="内容" size="large"></van-cell>
<van-cell title="单元格" value="内容" size="large" label="描述信息"></van-cell>
```

### 展示图标

通过`icon`属性在标题左侧展示图标。

```html
<van-cell title="单元格" icon="location-o" ></van-cell>
```

### 展示箭头

设置`is-link`属性后会在单元格右侧显示箭头，并且可以通过`arrow-direction`属性控制箭头方向。

```html
<van-cell title="单元格" is-link="true"></van-cell>
<van-cell title="单元格" is-link="true" value="内容"></van-cell>
<van-cell title="单元格" is-link="true" value="内容" arrow-direction="down"></van-cell>
```

### 分组标题

通过`CellGroup`的`title`属性可以指定分组标题。

```html
<van-cell-group title="分组1">
  <van-cell title="单元格" value="内容"></van-cell>
</van-cell-group>
<van-cell-group title="分组2">
  <van-cell title="单元格" value="内容"></van-cell>
</van-cell-group>
```

### 使用插槽

如以上用法不能满足你的需求，可以使用插槽来自定义内容。

```html
<van-cell value="内容" icon="shop-o" is-link="true">
  <div slot="title">
    <div class="van-cell-text">单元格</div>
    <van-tag type="danger">标签</van-tag>
  </div>
</van-cell>
<van-cell title="单元格">
  <van-icon slot="right-icon" name="search" class="custom-icon"></van-icon>
</van-cell>
```

### 垂直居中

通过`center`属性可以让`Cell`的左右内容都垂直居中。

```html
<van-cell center="true" title="单元格" value="内容" label="描述信息" ></van-cell>
```

## API

### CellGroup Props

| 参数           | 说明                   | 类型      | 默认值  |
| -------------- | ---------------------- | --------- | ------- |
| title          | 分组标题               | _string_  | `-`     |
| inset          | 是否展示为圆角卡片风格   | _boolean_ | `false` |
| border         | 是否显示外边框          | _boolean_ | `true`  |

### Cell Props

| 参数                 | 说明                                                       | 类型               | 默认值       |
| -------------------- | ---------------------------------------------------------- | ------------------ | ------------ |
| icon                 | 左侧图标名称或图片链接，可选值见 [Icon 组件](#/icon)       | _string_           | -            |
| title                | 左侧标题                                                   | _string \| number_ | -            |
| title-width          | 标题宽度，须包含单位                                       | _string_           | -            |
| value                | 右侧内容                                                   | _string \| number_ | -            |
| label                | 标题下方的描述信息                                         | _string_           | -            |
| size                 | 单元格大小，可选值为 `large`                               | _string_           | -            |
| border               | 是否显示下边框                                             | _boolean_          | `true`       |
| center               | 是否使内容垂直居中                                         | _boolean_          | `false`      |
| clickable            | 是否开启点击反馈                                           | _boolean_          | `false`      |
| is-link              | 是否展示右侧箭头并开启点击反馈                             | _boolean_          | `false`      |
| required             | 是否显示表单必填星号                                       | _boolean_          | `false`      |
| arrow-direction      | 箭头方向，可选值为 `left` `up` `down`                      | _string_           | -            |

### Cell Event

| 事件名     | 说明             | 参数 |
| ---------- | ---------------- | ---- |
| bind:tap   | 点击单元格时触发 | -    |
