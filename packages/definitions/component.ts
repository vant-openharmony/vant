import { EventEmitter } from '@@/utils';
import { EEValue } from './type';

type PropertyOptionToData<P extends PropertyOption> = {
  [name in keyof P]: PropertyToData<P[name]>;
};
type PropertyToData<T extends AllProperty> = T extends ShortProperty
  ? ValueType<T>
  : T extends ShortProperty[]
  ? ValueType<T[number]>
  : T extends AllFullProperty
  ? FullPropertyToData<Exclude<T, ShortProperty>>
  : null;

type FullPropertyToData<T extends AllFullProperty> = ValueType<T['type']>;
type PropertyType =
  | StringConstructor
  | NumberConstructor
  | BooleanConstructor
  | ArrayConstructor
  | ObjectConstructor
  | null;
type ValueType<T extends PropertyType> = T extends null
  ? any
  : T extends StringConstructor
  ? string
  : T extends NumberConstructor
  ? number
  : T extends BooleanConstructor
  ? boolean
  : T extends ArrayConstructor
  ? any[]
  : T extends ObjectConstructor
  ? IAnyObject
  : never;
type FullProperty<T extends PropertyType> = {
  /** 属性类型 */
  type: T;
  /** 属性初始值 */
  default?: ValueType<T>;
};
type AllFullProperty =
  | FullProperty<StringConstructor>
  | FullProperty<NumberConstructor>
  | FullProperty<BooleanConstructor>
  | FullProperty<ArrayConstructor>
  | FullProperty<ObjectConstructor>
  | FullProperty<null>;
type ShortProperty =
  | StringConstructor
  | NumberConstructor
  | BooleanConstructor
  | ArrayConstructor
  | ObjectConstructor
  | null;
type AllProperty = AllFullProperty | ShortProperty | ShortProperty[];
export type IAnyObject = Record<string, any>;
export type DataOption = Record<string, any>;
export type PropertyOption = Record<string, AllProperty>;
export type ComputedOption = Record<string, any>;
export type MethodOption = Record<string, Function>;
interface InstanceProperty<
  TData extends DataOption,
  TProperty extends PropertyOption
> {
  /** 组件数据，**包括内部数据和属性值** */
  data: TData & PropertyOptionToData<TProperty>;
  /** 组件数据，**包括内部数据和属性值**（与 `data` 一致） */
  props: TData & PropertyOptionToData<TProperty>;
}
export type TrivialInstance = Instance<
  IAnyObject,
  IAnyObject,
  IAnyObject,
  true,
  IAnyObject
>;
export type Instance<
  TData extends DataOption,
  TProperty extends PropertyOption,
  TMethod extends Partial<MethodOption>,
  TIsField extends boolean,
  TCustomInstanceProperty extends IAnyObject = {}
> = InstanceMethods<TData, TMethod> &
  TMethod &
  (TIsField extends true ? InstanceField : {}) &
  TCustomInstanceProperty &
  InstanceProperty<TData, TProperty>['data'];

interface InstanceMethods<
  D extends DataOption,
  M extends Partial<MethodOption>
> {
  /**
   * 持有注册过ref 属性的DOM元素或子组件实例的对象。示例见获取DOM元素。
   */
  $refs: IAnyObject;
  /**
   * 获得顶级ViewModel实例。获取ViewModel示例。
   */
  $root: IAnyObject;
  /**
   * 获得父级ViewModel实例。获取ViewModel示例。
   */
  $parent: IAnyObject;
  // $slots: IAnyObject;
  /**
   * 获得指定id的子级自定义组件的ViewModel实例。获取ViewModel示例。
   *
   * this.$child('xxx') ：获取id为xxx的子级自定义组件的ViewModel实例。
   */
  $child(id: string): IAnyObject;
  /**
   * 添加新的数据属性或者修改已有数据属性。
   */
  $set(key: string, value: any): void;
  /**
   * 删除数据属性。
   *
   * this.$delete('key')：删除数据属性。
   */
  $delete(key: string): void;
  /**
   * 获得指定id的组件对象，如果无指定id，则返回根组件对象。示例见获取DOM元素。
   */
  $element(id?: string): any;
  /** 触发事件，参见组件事件 */
  $emit<DetailType = any>(name: string, detail?: DetailType): void;
  $watch<C extends keyof M>(data: string, callback: C): void;
  setData$(data: Partial<D> & IAnyObject): void;
  readonly parent$?: TrivialInstance;
  readonly children$?: TrivialInstance[];
}

interface InstanceField {
  /** 在表单中的字段名 */
  name: string;
  /** 在表单中的字段值 */
  value: any;
}
export interface Data<D extends DataOption> {
  /** 组件的内部数据，和 `props` 一同用于组件的模板渲染 */
  data?: D | (() => D);
}
export interface Property<P extends PropertyOption> {
  /** 组件的对外属性，是属性名到属性设置的映射表 */
  props: P;
}

export interface Computed<C extends ComputedOption> {
  /** 组件的对外属性，是属性名到属性设置的映射表 */
  computed: C;
}
export interface Method<M extends MethodOption> {
  /** 组件的方法，包括事件响应函数和任意的自定义方法，关于事件响应函数的使用，参见 [组件间通信与事件](https://developers.weixin.qq.com/miniprogram/dev/framework/custom-component/events.html) */
  methods: M;
}

export interface IComponentLifetime {
  /**
   * 初始化自定义组件
   *
   * 自定义组件初始化生命周期回调，当自定义组件创建时，触发该回调，主要用于自定义组件中必须使用的数据初始化，该回调只会触发一次调用。
   */
  onInit(): void | Promise<void>;
  /**
   * 自定义组件装载
   *
   * 自定义组件被创建后，加入到Page组件树时，触发该回调，该回调触发时，表示组件将被进行显示，该声明周期可用于初始化显示相关数据，通常用于加载图片资源、开始执行动画等场景。
   */
  onAttached(): void | Promise<void>;
  /**
   * 自定义组件布局完成
   *
   * 自定义组件插入Page组件树后，将会对自定义组件进行布局计算，调整其内容元素尺寸与位置，当布局计算结束后触发该回调。
   */
  onLayoutReady(): void | Promise<void>;
  /**
   * 自定义组件摘除
   *
   * 自定义组件摘除时，触发该回调，常用于停止动画或异步逻辑停止执行的场景。
   */
  onDetached(): void | Promise<void>;
  /**
   * 自定义组件销毁
   *
   * 自定义组件销毁时，触发该回调，常用于资源释放。
   */
  onDestroy(): void | Promise<void>;
  /**
   * 自定义组件Page显示
   *
   * 自定义组件所在Page显示后，触发该回调。
   */
  onPageShow(): void | Promise<void>;
  /**
   * 自定义组件Page隐藏
   *
   * 自定义组件所在Page隐藏后，触发该回调。
   */
  onPageHide(): void | Promise<void>;
}

export interface RelationOption {
  /** 目标组件的相对关系 */
  type: 'ancestor' | 'descendant';
  /** 关系生命周期函数，当关系被建立在页面节点树中时触发，触发时机在组件attached生命周期之后 */
  linked?(target: TrivialInstance): void;
  /** 关系生命周期函数，当关系在页面节点树中发生改变时触发，触发时机在组件moved生命周期之后 */
  linkChanged?(target: TrivialInstance): void;
  /** 关系生命周期函数，当关系脱离页面节点树时触发，触发时机在组件detached生命周期之后 */
  unlinked?(target: TrivialInstance): void;
  /** 如果这一项被设置，则它表示关联的目标节点所应具有的behavior，所有拥有这一behavior的组件节点都会被关联 */
  // target?: string
}

export interface Lifetimes {
  lifetimes: Partial<IComponentLifetime>;
}
export interface ZapOption {
  behaviors: string[];
  relations: Record<string, RelationOption>;
}
export interface OtherOption {
  relation?: {
    relations: ZapOption['relations'];
    mixin: string;
  };
}

export type ComponentOptions<
  TData extends DataOption,
  TProperty extends PropertyOption,
  TComputed extends ComputedOption,
  TMethod extends MethodOption,
  TIsField extends boolean,
  TCustomInstanceProperty extends IAnyObject = {}
> = Partial<Data<TData>> &
  Partial<Property<TProperty>> &
  Partial<Computed<TComputed>> &
  Partial<Method<TMethod>> &
  Partial<Lifetimes> &
  Partial<OtherOption> &
  ThisType<
    Instance<TData, TProperty, TMethod, TIsField, TCustomInstanceProperty>
  > & {
    name: string;
    field?: TIsField;
  };
