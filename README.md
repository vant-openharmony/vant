<p align="center">
  <img alt="logo" src="https://img.yzcdn.cn/vant/logo.png" width="120" style="margin-bottom: 10px;">
</p>
<h3 align="center">轻量、可靠的 openharmony UI 组件库</h3>

<p align="center">
  <img src="https://img.shields.io/npm/v/oh-vant.svg?style=for-the-badge" alt="npm version" />
  <img src="https://img.shields.io/badge/License-MIT-blue.svg?style=for-the-badge&color=#4fc08d" />
  <img src="https://img.shields.io/npm/dt/oh-vant.svg?style=for-the-badge&color=#4fc08d" alt="downloads" />
  <img src="https://img.shields.io/npm/dm/oh-vant.svg?style=for-the-badge&color=#4fc08d" alt="downloads" />
</p>

<p align="center">
  🔥 <a href="https://vant-openharmony.gitee.io/vant">文档网站</a>
  &nbsp;
  🌈 <a href="https://vant-contrib.gitee.io/vant-weapp">小程序版</a>
  &nbsp;
  🚀 <a href="https://github.com/youzan/vant" target="_blank">Vue 版</a>
</p>

---

### 介绍

Vant 是**有赞前端团队**开源的移动端组件库，于 2016 年开源，已持续维护 4 年时间。Vant 对内承载了有赞所有核心业务，对外服务十多万开发者，是业界主流的移动端组件库之一。

目前 Vant 官方提供了 [Vue 版本](https://vant-contrib.gitee.io/vant)和[微信小程序版本](http://vant-contrib.gitee.io/vant-weapp)，并由社区团队维护 [React 版本](https://github.com/mxdi9i7/vant-react) 和 [Openharmony 版本](https://gitee.com/vant-openharmony/vant)。

### 预览

Clone [组件库示例仓储](https://gitee.com/vant-openharmony/example)， 使用DevEco自带的Previewer工具可以预览进行体验组件库示例。

### 快速上手

请参考 [快速上手](https://vant-openharmony.gitee.io/vant/#/quickstart)

### 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/vant-openharmony/vant/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/vant-openharmony/vant/pulls)

### 链接

- [意见反馈](https://gitee.com/vant-openharmony/vant/issues)
- [更新日志](https://vant-openharmony.gitee.io/vant/#/changelog)
- [Vant Vue 版](https://github.com/youzan/vant)
- [Vant 小程序版](https://github.com/youzan/vant-weapp)

### TODO

- `create-vant-cli-openharmony` -- `JS FA`脚手架，通过脚手架快速创建以 TS+LESS+HML 标准的FA项目
- `openharmony-api-typings` -- 提炼 [VantComponent](https://gitee.com/vant-openharmony/vant/blob/master/packages/common/component.ts#L14) 的功能，打造高可用的 `Component` 和 `Page` 的 openharmony 版本。（API借鉴微信小程序）
- 未实现的 `Vant Web` 组件

### 开源协议

本项目基于 [MIT](https://zh.wikipedia.org/wiki/MIT%E8%A8%B1%E5%8F%AF%E8%AD%89) 协议，请自由地享受和参与开源
